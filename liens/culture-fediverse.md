## Créations autour du Fédiverse
* [Musique électronique par CookieRamen : Fediverse](https://soundcloud.com/cokram/fediverse)
* [poème parodique en anglais sur le capitalisme de surveillance](https://cybre.space/@lifning/98835409973873638)
* [site compagnon de mastodon, un répertoire et moteur de recherche qui agrège les #ironèmes](https://ironemes.eu.org/search)
* [site compagnon de mastodon qui agrège les #mercrediFiction](https://mercredifiction.bortzmeyer.org/)
* [un webcomic à plusieurs auteurs/autrices qui s’intitule : « Tales from the fediverse »](https://exilian.co.uk/projects/FediverseTales1.pdf)
  
## Articles ou analyses sur le Fédiverse
* [Pourquoi Mastodon se moque de la « masse critique »](https://framablog.org/2018/11/14/la-fediverse-cest-pas-une-starteupe/)
* [How the biggest decentralized social network is dealing with its Nazi problem (The Verge, 12/07/2019)](https://www.theverge.com/2019/7/12/20691957/mastodon-decentralized-social-network-gab-migration-fediverse-app-blocking)
  
## Ressources statistiques  
* [the.federation.info](https://the-federation.info/)