* décider du genre de Fediverse le/la/lae ?
* revoir la traduction de concepts comme "agonisme"
* revoir avec Framatophe la présentation correcte des notes infrapaginales
* surveiller ce qui pourra s’échanger sur le protocole ActivityPub pendant [ces conférences distantes](https://conf.activitypub.rocks/#home) 2 au 5 octobre 2020

Remarques de Framatophe 01/07/2020 : 

J'ai remis les références (à peu près) conformes aux normes françaises.

Je ne suis pas fan de al traduction systématique des titre d'articles ou de livre qui n'ont pas été traduits en français. D'une part parce qu'on est pas sûr que c'est cette traduction qui serait choisie en ca de traduction de l'oeuvre et d'autre part, cela fausse le référencement.  Je les ai laissées en utilisant mais je propose de supprimer.

Numérotation des section : ne pas numéroter. Le style suffit. Un numéro de section doit se faire en utilisant les styles. Par exemple si quelqu'un transforme ce fichier markdown en html, tex ou .odt, il applique un style avec numérotation de section (la première section valant introduction). Ici, les sections sont en niveau de titre 2.

J'ai placé des insécables là où il fallait (en général)

Appels de notes : pas d'espace ni de ponctuation entre le mot et l'appel de note.

je n'ai pas fait de correction ortho/gramm (je n'ai pas relu).
