# Quoi c'est ?

C'est le texte de la ligne éditoriale de ToF.

# Comment qu'on fait ?

On modifie uniquement le fichier ``base.md``.

Ensuite on passe une ligne de commande qui utilise pandoc : 

``pandoc -s --toc base.md -o base.html``

Puis on copie/colle le code HTML voulu dans l'espace dédié de ``structure.html``
pour créer un fichier ``index.html`` qu'on place sur le serveur.

# Quékia encore à faire ?

Traduire en angliche et voir comment on affiche le résultat en choissant la langue.
