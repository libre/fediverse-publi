---
title: "À propos de Tales of the Fediverse"

---


À propos de Tales of the Fediverse
==================================


Pourquoi ce projet ?
-------------------------


Les droits et les libertés sur Internet évoluent selon un équilibre
fragile. D'un côté les GAFAM cherchent à centraliser les contenus et se
nourrissent de publicité et des données personnelles tout en sacrifiant
nos vies privées sur l'autel du capitalisme de surveillance. D'un
autre côté, les États résistent difficilement à la tentation de la
surveillance de masse et du contrôle d'autant plus intrusifs que les
technologies se prêtent eu jeu du tri social, de la coercition et des
discriminations.

Ces dernières années, les critiques vis-à-vis des plateformes ont fini
par créer un contexte favorable à l'émergence d'alternatives
sérieuses, profondément attachées à la démocratie et à une vision
éthique des usages d'Internet, des infrastructures et des technologies
de communication. De nouveaux protocoles ont vu le jour, des communautés
d'utilisateurs se sont formées, tout en plébiscitant les logiciels
libres et l'ouverture de ces protocoles comme ce fut le cas aux
origines de l'Internet il y a plus de quarante ans.

Le Fédiverse en est une illustration éclatante. Il s'agit d'un
ensemble de protocoles et d'applications permettant de publier des
contenus web sur des serveurs fédérés entre eux. Le Fédiverse est
dynamique. De nombreuses instances de services naissent chaque jour,
qu'elles relèvent d'initiatives personnelles ou d'organisations. Ces
services ne sont pas de simples alternatives à ceux des GAFAM : en plus
de proposer des conditions optimales, égalitaires et non-intrusives de
partages de contenus, il s'agit de déconstruire le modèle marchand de
la concentration des ressources et de la captation de l'attention, tout
en promouvant les principes d'ouverture, de partage, de liberté
d'expression et de démocratie.

Mastodon, Diaspora, GNUSocial, Friendica, Hubzilla, Peertube, Plume,
WriteFreely, Funkwhale, Pleroma… autant de projets, autant de
potentiel créatif. Le Fédiverse est dynamique ! Déjà les retours
d'expériences sont riches d'enseignement. Échecs et réussites ne se
mesurent pas à l'aune du nombre d'utilisateurs. Les applications du
Fédiverse allient la technique et l'éthique *by design*.

Aujourd'hui, il est important de pouvoir échanger de manière formelle
et en profondeur sur les aspects techniques et éthiques du Fédiverse. Il
est important de mieux comprendre où nous mènent toutes ces initiatives
et innovations. Il est important de réfléchir à leur avenir. Le
Fédiverse sera ce que nous en ferons.

**Dans ce but, le projet Tales of the Fediverse (ToF) propose un espace
d'échanges et de discussions par textes interposés afin d'enrichir les
réflexions et laisser libre cours à toutes les conceptions que les
auteurs et autrices voudront bien déposer. Et où serait-il le plus opportun
d'échanger si ce n'est sur le Fédiverse ? C'est pourquoi nous
proposons l'utilisation d'un moteur de blog fédéré
([Plume](https://joinplu.me/)).**



Qu'est-ce que l'instance ToF ?
--------------------------------

Le projet Tales of Fediverse (ToF) vise à publier des textes sur une
instance de Plume. Celle-ci est maintenue par **Framasoft**.

Plume est une application de blogging fédérée. Il est possible d'y
écrire en utilisant la syntaxe markdown ou HTML (entre autres).

Comment participer ?
--------------------

**Que ce soit sur cette instance de Plume ou sur une autre, chacun peut
participer. Aucune date limite n'est fixée.**

Il s'agit de publier tout texte d'intérêt portant sur les sujets cités
ci-dessus (et d'autres encore), en lien avec le Fédiverse. Qu'elles
soient académiques ou non, analytiques ou poétiques, théoriques ou
pratiques, toutes les approches sont les bienvenues pourvu qu'elles
satisfassent aux quelques contraintes formelles suivantes.

Chaque texte dispose d'un résumé (600 caractères max.) et d'une série
de mots-clés à renseigner obligatoirement (pas plus de 8 mots-clés par
texte).

Pour se repérer dans les flux d'articles, il est demandé d'entrer le
mot-clé `#ToF` dans les publications concernées.

**Quelques mots-clés (ordre alphabétique) :** activitypub,
administration, aspects juridiques, avenir du Fediverse, backup, bonnes
pratiques, chiffrement, choix des instances, choix éthiques, choix
techniques, décentralisation, économie, fédérer/défédérer, financement,
gestion des clés, gouvernance, interopérabilité, liberté d'expression,
médias sociaux, maintenance, modération, OStatus, P2P, philosophie,
portabilité des comptes, réseaux sociaux, sauvegarde, SSO, standards
ouverts, webfinger, XMPP...

Comment faire ?
---------------

Vous pouvez participer :

-   sur votre propre instance si vous en disposez,
-   sur n'importe quelle instance [de votre choix](https://joinplu.me/),
-   sur l'[instance Tof](https://tof.framasoft.org) de
    **Framasoft** (réservée uniquement à des articles relevant du projet
    ToF).

Ouvrez ensuite un blog (si ce n'est déjà fait) et publiez votre
article.

Un article peut avoir un ou plusieurs auteurs. Chaque auteur peut écrire
en son nom propre ou sous un pseudonyme.

Faites un brouillon, relisez-vous ou faites-vous relire. C'est
important pour les lecteurs et pour vous-même.

L'écriture inclusive est permise et même encouragée, mais non
obligatoire.

Il n'y a pas de limite de caractères pour les articles. Cela dit, 
si votre article est très long, il peut être préférable 
de le publier en plusieurs parties.

**Le modèle :**

-   Titre de l'article : dans le champ « titre ».
-   Résumé : 600 caractères max à copier-coller dans le champ «
    sous-titre ».
-   Mots-clés (8 max.) : à indiquer lorsque vous enregistrerez
    l'article (prévoyez-les en amont).
-   N'oubliez pas le mot-clé `#ToF`.
-   Auteur-trice (s) : soit vous vous contentez de votre identifiant
    donné lors de l'inscription sur l'instance, soit vous prévoyez un
    petit bloc en bas de l'article pour parler de vous, indiquer
    quelques éléments biographiques.
-   S'il y a plusieurs auteurs, indiquez en haut de l'article une
    formulation explicite.
-   Prévoyez une licence libre de préférence pour votre publication, par
    exemple : Licence Art Libre, CC By-Sa...
-   Les images : si possible, ne multipliez pas les images inutiles qui
    pourraient nuire à la compréhension de votre travail. Pensez aussi
    au lectorat !

Quelle modération ?
-------------------

Les principes de modération de l'instance
[tof.framasoft.org](https://tof.framasoft.org) sont expliqués sur la
page « [À propos](https://tof.framasoft.org/about) » de l'instance.
Pour les autres instances, reportez-vous aux pages concernées.

